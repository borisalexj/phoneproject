package com.epam.rd.advphone.views;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.epam.rd.advphone.R;
import com.epam.rd.advphone.databinding.FragmentKeypadBinding;

public class KeypadFragment extends Fragment implements View.OnClickListener {
    private FragmentKeypadBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_keypad, container, false);

        setOnclickListeners();

        return binding.getRoot();
    }

    public void callFirstSim(View view) {
        String toDial = "tel:" + binding.editNumber.getText().toString();
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(toDial)));
        if (ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
    }

    public void callSecondSim(View view) {
        String toDial = "tel:" + binding.editNumber.getText().toString();
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(toDial)));
        if (ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
    }

    private void setOnclickListeners() {
        binding.buttonNumberOne.setOnClickListener(this);
        binding.buttonNumberTwo.setOnClickListener(this);
        binding.buttonNumberThree.setOnClickListener(this);
        binding.buttonNumberFour.setOnClickListener(this);
        binding.buttonNumberFive.setOnClickListener(this);
        binding.buttonNumberSix.setOnClickListener(this);
        binding.buttonNumberSeven.setOnClickListener(this);
        binding.buttonNumberEight.setOnClickListener(this);
        binding.buttonNumberNine.setOnClickListener(this);
        binding.buttonNumberZero.setOnClickListener(this);
        binding.buttonAsterisk.setOnClickListener(this);
        binding.buttonSharp.setOnClickListener(this);
        binding.imageButtonBackspase.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageButton_backspase:
                String text = binding.editNumber.getText().toString();
                if (text.length() > 0) {
                    binding.editNumber.setText(text.substring(0, text.length() - 1));
                }
                break;
            default:
                binding.editNumber.setText(binding.editNumber.getText() + view.getTag().toString());
                break;
        }
    }
}
