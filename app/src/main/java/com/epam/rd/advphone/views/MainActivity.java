package com.epam.rd.advphone.views;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.epam.rd.advphone.Constants;
import com.epam.rd.advphone.R;
import com.epam.rd.advphone.adapters.MainViewPager2Adapter;
import com.epam.rd.advphone.databinding.ActivityMainBinding;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding mainBinding;
    private MainViewPager2Adapter viewPager2Adapter;
    private SharedPreferences preferences;
    private int[] tabTitles;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(mainBinding.toolBar);

        initTabTitles();

        initTabLayoutAndViewPager();

        String tabPanelLocationgit = preferences.getString(Constants.PANEL_LOCATION, getString(R.string.tag_location_top));
    }

    private void initTabLayoutAndViewPager() {
        viewPager2Adapter = new MainViewPager2Adapter(getSupportFragmentManager(), getLifecycle());
        mainBinding.viewPager2.setAdapter(viewPager2Adapter);

        preferences = getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, MODE_PRIVATE);

        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(mainBinding.tabLayout,
                mainBinding.viewPager2, (tab, position) -> {
            tab.setText(tabTitles[position]);
        });
        mainBinding.tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        tabLayoutMediator.attach();
    }

    private void initTabTitles() {
        tabTitles = new int[] {R.string.keypad, R.string.recent, R.string.contacts};
    }

    //init main menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.settingsItem){
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
